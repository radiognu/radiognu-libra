#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  Libra - Script de llamada asíncrona que entrega la siguiente canción a
#          reproducir por Liquidsoap.
#
#  Version 0.1.0-beta
#
#  Copyleft 2014 - 2015 Felipe Peñailillo Castañeda <breadmaker@radiognu.org>
#
#  Este programa es software libre; puede redistribuirlo y/o modificarlo bajo
#  los términos de la Licencia Pública General GNU tal como se publica por
#  la Free Software Foundation; ya sea la versión 3 de la Licencia, o
#  (a su elección) cualquier versión posterior.
#
#  Este programa se distribuye con la esperanza de que le sea útil, pero SIN
#  NINGUNA GARANTÍA; sin incluso la garantía implícita de MERCANTILIDAD o
#  IDONEIDAD PARA UN PROPÓSITO PARTICULAR. Vea la Licencia Pública
#  General de GNU para más detalles.
#
#  Debería haber recibido una copia de la Licencia Pública General de GNU
#  junto con este programa; de lo contrario escriba a la Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor,
#  Boston, MA 02110-1301, EE. UU.

#  ATENCIÓN: ESTA ES UNA PRUEBA DE CONCEPTO EN DESARROLLO
#  Recomendamos bien leer los comentarios antes de aportar con código
#  Tambien recomendamos ver https://gitlab.com/radiognu/radiognu-updb, para
#  conocer la estructura de la base de datos del repositorio musical

# Importamos la configuración
from libra_config import PLAYLIST_FILE
# Importamos choice para elegir un elemento de la lista al azar
from random import choice
# dataset permite manipular bases de datos de manera sencilla
# datetime se usa para calcular la diferencia entre dos fechas
# argparse permite configurar e interpretar fácilmente los argumentos recibidos
import dataset, datetime, argparse, simplejson as json

# Configurando los argumentos recibidos
argParser = argparse.ArgumentParser(description="Mantiene equilibrada la"
    " aleatoridad de los archivos a reproducir por Liquidsoap.",
    add_help=False)
# Asignando nuestro propio mensaje de ayuda
argParser.add_argument("-h", "--help", action="help",
    help="Muestra este mensaje de ayuda.")
# Definiendo el argumento para verificar si la canción esta disponible usando
# la id interna de la base de datos
# NOTA: La variable del argumento debe ser de tipo entero
argParser.add_argument("-i", "--id", type=int, metavar="ID",
    help="Intenta reproducir la canción identificada con el id ID.")
# Definiendo el argumento para indicar que será el script externo
# quien se encargue de actualizar la información relacionada con
# la última vez reproducida
argParser.add_argument("-e", "--external-update", dest="external",
    action="store_true",
    help="La actualización de las horas de reproducción son manejadas por el script que llama a LIBRA.")
argParser.add_argument("-n", "--no-external-update", dest="external",
    action="store_false",
    help="La actualización de las horas de reproducción son manejadas por LIBRA (por defecto).")
argParser.set_defaults(external=False)
# Definiendo un argumento para verificar si la canción está disponible usando la
# ruta del archivo
argParser.add_argument("-p", "--path", type=int, metavar="/ruta/al/archivo.ogg",
    help="Intenta reproducir la canción que está en /ruta/al/archivo.ogg.")
# Iniciamos el analizador de argumentos
args = argParser.parse_args()

# Variable global para almacenar las razones por la que una selección
# ha sido denegada

denied_reason = []

# Actualiza la fecha de la última vez que se tocó una canción
# db es el puntero a la base de datos
# path es la ruta de la canción seleccionada
def update_last_played_song(db, path):
    selected_song_info = db['song'].find_one(song_path=path)
    # Creando una variable para guardar los resultados de la búsqueda
    data_to_update = []
    # Guardando la fecha y hora actuales
    actual_datetime = datetime.datetime.today()
    # Actualizando el dato de última vez reproducida en la fila correspondiente
    db['song'].update(dict(song_id = selected_song_info['song_id'],
        song_last_played = actual_datetime), ['song_id'])
    # Guardando los cambios
    db.commit()

# Actualiza la fecha de la última vez que se tocó un álbum
# db es el puntero a la base de datos
# path es la ruta de la canción seleccionada
def update_last_played_album(db, path):
    selected_song_info = db['song'].find_one(song_path=path)
    # Creando una variable para guardar los resultados de la búsqueda
    data_to_update = []
    # Guardando la fecha y hora actuales
    actual_datetime = datetime.datetime.today()
    # Actualizando el dato de última vez reproducida en la fila correspondiente
    db['album'].update(dict(album_id = selected_song_info['album_id'],
        album_last_played = actual_datetime), ['album_id'])
    # Guardando los cambios
    db.commit()

# Actualiza la fecha de la última vez que se tocó un artista
# db es el puntero a la base de datos
# path es la ruta de la canción seleccionada
def update_last_played_artist(db, path):
    selected_song_info = db['song'].find_one(song_path=path)
    # Creando una variable para guardar los resultados de la búsqueda
    data_to_update = []
    # Guardando la fecha y hora actuales
    actual_datetime = datetime.datetime.today()
    # Actualizando el dato de última vez reproducida en la fila correspondiente
    db['artist'].update(dict(artist_id = selected_song_info['artist_id'],
        artist_last_played = actual_datetime), ['artist_id'])
    # Guardando los cambios
    db.commit()

# Verifica si el artista está disponible
def is_artist_available(selected_song_path):
    global denied_reason
    # Obteniendo la información del artista
    selected_song_info = db['song'].find_one(song_path=selected_song_path)
    # Buscando si ese artista ya tiene una última fecha de reproducción
    selected_artist_last_played = db['artist'].find_one(artist_id =
        selected_song_info['artist_id'])['artist_last_played']
    # Obteniendo la diferencia entre la fecha de la última vez y la
    # actual para el artista
    selected_artist_last_played_timedelta = \
        datetime.datetime.today() - selected_artist_last_played
    # Si han pasado más de 14400 segundos (4 horas), entonces el artista
    # está disponible para ser reproducido
    selected_artist_last_played_timedelta_seconds = \
        (selected_artist_last_played_timedelta.microseconds +
        (selected_artist_last_played_timedelta.seconds +
        selected_artist_last_played_timedelta.days * 24 * 3600) * 10**6) \
        / 10**6
    if selected_artist_last_played_timedelta_seconds > 14400:
        # Hemos terminado.
        return True
    # De lo contrario, el artista no está disponible de momento
    else:
        denied_reason.append({"artist_cooldown":
            int(14400 - selected_artist_last_played_timedelta_seconds)})
        return False

# Verifica si el álbum está disponible
def is_album_available(selected_song_path):
    global denied_reason
    # Obteniendo la información del artista
    selected_song_info = db['song'].find_one(song_path=selected_song_path)
    # Buscando si ese álbum ya tiene una última fecha de reproducción
    selected_album_last_played = db['album'].find_one(album_id =
        selected_song_info['album_id'])['album_last_played']
    # Obteniendo la diferencia entre la fecha de la última vez y la
    # actual para el álbum
    selected_album_last_played_timedelta = \
        datetime.datetime.today() - selected_album_last_played
    # Si han pasado más de 28800 segundos (8 horas), entonces el álbum
    # está disponible para ser reproducido
    selected_album_last_played_timedelta_seconds = \
        (selected_album_last_played_timedelta.microseconds +
        (selected_album_last_played_timedelta.seconds +
        selected_album_last_played_timedelta.days * 24 * 3600) * 10**6) \
        / 10**6
    if selected_album_last_played_timedelta_seconds > 28800:
        # Hemos terminado.
        return True
    # De lo contrario, el álbum no está disponible de momento
    else:
        denied_reason.append({"album_cooldown":
            int(28800 - selected_album_last_played_timedelta_seconds)})
        return False

# Verifica si la canción está disponible
def is_song_available(selected_song_path):
    global denied_reason
    # Obteniendo la información del artista
    selected_song_info = db['song'].find_one(song_path=selected_song_path)
    # Buscando si esa canción ya tiene una última fecha de reproducción
    selected_song_last_played = selected_song_info['song_last_played']
    # Obteniendo la diferencia entre la fecha de la última vez y la
    # actual para la canción
    selected_song_last_played_timedelta = \
        datetime.datetime.today() - selected_song_last_played
    # Si han pasado más de 43200 segundos (12 horas), entonces la canción
    # está disponible para ser reproducida
    selected_song_last_played_timedelta_seconds = \
        (selected_song_last_played_timedelta.microseconds +
        (selected_song_last_played_timedelta.seconds +
        selected_song_last_played_timedelta.days * 24 * 3600) * 10**6) \
        / 10**6
    if selected_song_last_played_timedelta_seconds > 43200:
        # Hemos terminado.
        return True
    # De lo contrario, la canción no está disponible de momento
    else:
        denied_reason.append({"song_cooldown":
            int(43200 - selected_song_last_played_timedelta_seconds)})
        return False

# Conectando a la base de datos definida en la configuración
db = dataset.connect()

# Verificando si se ha invocado el script con el argumento --id ID
if args.id:
    selected_song_from_args = db['song'].find_one(song_id=args.id)
    if selected_song_from_args:
        selected_song_path = selected_song_from_args['song_path']
        if is_song_available(selected_song_path) and \
            is_album_available(selected_song_path) and \
            is_artist_available(selected_song_path):
            if not args.external:
                # Guardando la última fecha de reproducción para tal artista
                update_last_played_artist(db, selected_song_path)
                # Guardando la última fecha de reproducción para tal álbum
                update_last_played_album(db, selected_song_path)
                # Guardando la última fecha de reproducción para tal canción
                update_last_played_song(db, selected_song_path)
            # Devolviendo el ID de la canción
            print(json.dumps({"status": 0, "id": args.id}))
            # Hemos terminado.
            exit(0)
        else:
            print(json.dumps({"status": 1, "reason": denied_reason}))
            # Hemos terminado.
            exit(1)
    else:
        raise RuntimeError("ID no válida")
if args.path:
    selected_song_from_args = db['song'].find_one(song_path=args.path)
    if selected_song_from_args:
        selected_song_path = selected_song_from_args['song_path']
        if is_song_available(selected_song_path) and \
            is_album_available(selected_song_path) and \
            is_artist_available(selected_song_path):
            if not args.external:
                # Guardando la última fecha de reproducción para tal artista
                update_last_played_artist(db, selected_song_path)
                # Guardando la última fecha de reproducción para tal álbum
                update_last_played_album(db, selected_song_path)
                # Guardando la última fecha de reproducción para tal canción
                update_last_played_song(db, selected_song_path)
            # Devolviendo la uri de la canción
            print("OK")
            # Hemos terminado.
            exit(0)
        else:
            # En teoría, la lógica no debería llegar a aeste punto, con
            # todas las validaciones previas ejecutadas.
            print(json.dumps({"status": 1, "reason": denied_reason}))
            # Hemos terminado.
            exit(1)
    else:
        raise RuntimeError("Ruta no válida")
else:
    # Abrimos y leemos de inmediato la lista de reproducción
    playlist = open(PLAYLIST_FILE).readlines()

    # Repitiendo hasta que encontremos una canión disponible para reproducir
    while(True):
        # Seleccionando al azar una canción de la lista
        selected_song_path = choice(playlist).rstrip()
        if is_artist_available(selected_song_path) and \
            is_album_available(selected_song_path) and \
            is_song_available(selected_song_path):
            # Devolviendo la uri de la canción a Liquidsoap
            print(selected_song_path.encode('utf-8'))
            # Guardando la última fecha de reproducción para tal artista
            update_last_played_artist(db, selected_song_path)
            # Guardando la última fecha de reproducción para tal álbum
            update_last_played_album(db, selected_song_path)
            # Guardando la última fecha de reproducción para tal canción
            update_last_played_song(db, selected_song_path)
            # Hemos terminado.
            exit(0)
